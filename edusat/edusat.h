#pragma once
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <sstream>  // stringstream allows converting ints and floats into strings. 
#include <fstream>
#include <cassert>
#include <ctime>

using namespace std;

typedef int Var;
typedef int Lit;
typedef vector<Lit> clause_t;
typedef clause_t::iterator clause_it;
typedef vector<Lit> trail_t;

#define Assert(exp) AssertCheck(exp, __func__, __LINE__)


#define Neg(l) (l & 1)
#define Restart_multiplier 1.1f
#define Restart_lower 100
#define Restart_upper 1000
#define Max_bring_forward 10
#define var_decay 0.99
#define clause_decay 0.999
#define Rescale_threshold 1e100

#define Assignment_file "assignment.txt"

int verbose = 0;
double begin_time;
unsigned int timeout = 0;
unsigned int ReduceClauses = 5000;

enum class VAR_DEC_HEURISTIC {
	MINISAT, 
	/* Each time a clause is learned, we push to the end of the list the learned clause + some clauses 
	that participated in its learning. Traversing these clauses from the end, if a clause has at least 
	one unassigned literal we give it a value according to the value heuristic.
	Looking at only unresolved clauses makes the search better but more expensive to decide, and overall worse. */
	CMTF } ;

VAR_DEC_HEURISTIC VarDecHeuristic = VAR_DEC_HEURISTIC::MINISAT;

enum class VAL_DEC_HEURISTIC {
	/* Same as last value. Initially false*/
	PHASESAVING, 
	/* Choose literal with highest frequency */
	LITSCORE 
} ;

VAL_DEC_HEURISTIC ValDecHeuristic = VAL_DEC_HEURISTIC::PHASESAVING;


enum class Reduce_Heuristic {
	No_Reduce, 
	LBD3_ClauseActivity, // lbd <= 3 stay. Worst half, based on clause activity, is removed. 
	LBD3_BCPActivity, // lbd <= 3 stay. Worst half, based on BCP activity (how many times the clause became a reason), is removed. 
	LBD3_BCPWActivity // lbd <= 3 stay. Worst half, based on how many literals were implied by the literal that this is its reason clause.
};

Reduce_Heuristic ReduceHeuristic = Reduce_Heuristic::LBD3_ClauseActivity;

// Option = <pointer to var, lower-bound, upper-bound, msg>
typedef tuple<void*, int, int, string> option;
unordered_map<string, option> options = {
	{"v",			option(&verbose,0,2,"Verbosity level")},
	{"timeout",		option(&timeout, 0, 36000, "Timeout in seconds")},
	{"vardh",		option(&VarDecHeuristic, 0, 1,"{0: minisat, 1: clause-move-to-front}")},
	{"valdh",		option(&ValDecHeuristic, 0, 1, "{0: phase-saving, 1: literal-score}")},
	{"reduce",		option(&ReduceHeuristic, 0, 3, "Reduce heuristic. {0: no-reduce, 1: lbd3+activity , 2: lbd3+bcp activity, 3: lbd3 + bcp-weighted activity}")},
	{"reduceclauses",option(&ReduceClauses, 0, 20000, "Number of learned clauses before reduce")}	
};

enum class LitState {
	L_UNSAT,
	L_SAT,
	L_UNASSIGNED
};

enum class VarState {
	V_FALSE,
	V_TRUE,
	V_UNASSIGNED
};

enum class ClauseState {
	C_UNSAT,
	C_SAT,
	C_UNIT,
	C_UNDEF
};

enum class SolverState{
	UNSAT,
	SAT,
	CONFLICT, 
	UNDEF,
	TIMEOUT
} ;


/***************** service functions **********************/

#ifdef _MSC_VER
#include <ctime>

static inline double cpuTime(void) {
    return (double)clock() / CLOCKS_PER_SEC; }
#else

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

static inline double cpuTime(void) {
    struct rusage ru;
    getrusage(RUSAGE_SELF, &ru);
    return (double)ru.ru_utime.tv_sec + (double)ru.ru_utime.tv_usec / 1000000; }
#endif

// For production wrap with #ifdef _DEBUG
void AssertCheck(bool cond, string func_name, int line, string msg = "") {
	if (cond) return;
	cout << "Assertion fail" << endl;
	cout << msg << endl;
	cout << func_name << " line " << line << endl;
	exit(1);
}

bool match(ifstream& in, char* str) {
    for (; *str != '\0'; ++str)
        if (*str != in.get())
            return false;
    return true;
}

unsigned int Abs(int x) { // because the result is compared to an unsigned int. unsigned int are introduced by size() functions, that return size_t, which is defined to be unsigned. 
	if (x < 0) return (unsigned int)-x;
	else return (unsigned int)x;
}

unsigned int v2l(int i) { // maps a literal as it appears in the cnf to literal
	if (i < 0) return ((-i) << 1) - 1; 
	else return i << 1;
} 

Var l2v(Lit l) {
	return (l+1) / 2;	
} 

Lit opposite(Lit l) {
	if Neg(l) return l + 1;  // odd
	return l - 1;		
}

int l2rl(int l) {
	return Neg(l)? -((l + 1) / 2) : l / 2;
}


/********** classes ******/ 

class Clause {
	clause_t c;
	short lw,rw; //watches;
	int prev, next; // indices in cnf of the prev and next clause according to the current order (the order changes in cmtf). 
	short lbd;
	bool deleted; // !!
	double activity; // in conflict generation
	double bcp_activity; // in bcp (# of times this clause was a reason)
	double bcpWeighted_activity; // in bcp (commulative number of literals implied via BCP by literals in this clause)
public:	
	Clause(): deleted(false), activity(0.0), bcp_activity(0.0), bcpWeighted_activity(0.0) {};
	inline void insert(int i) {c.push_back(i);}
	inline void lw_set(int i) {lw = i; /*assert(lw != rw);*/}
	inline void rw_set(int i) {rw = i; /*assert(lw != rw);*/}
	inline void prev_set(int i) {prev = i;}
	inline void next_set(int i) {next = i;}
	inline void lbd_set(short i) { lbd = i; }
	inline void activity_add(double val) { activity += val;}
	inline void bcp_activity_add(double val) { bcp_activity += val; }
	inline void bcpWeighted_activity_add(double val) { bcpWeighted_activity += val; }
	clause_t& cl()		{return c;}
	short get_lw()		{return lw;}
	short get_rw()		{return rw;}
	int get_lw_lit()	{return c[lw];}
	int get_rw_lit()	{return c[rw];}
	int get_prev()		{return prev;}
	int get_next()		{return next;}
	short get_lbd()		{return lbd;}
	double get_activity() {return activity;}
	double get_bcp_activity() { return bcp_activity;}
	double get_bcpWeighted_activity() {	return bcpWeighted_activity;}
	inline int lit(int i) {return c[i];}
	inline ClauseState next_not_false(bool is_left_watch, Lit other_watch, bool binary, short& loc); 
	size_t size() {return c.size();}	
	void reset() { c.clear(); }	
	void print() {for (clause_it it = c.begin(); it != c.end(); ++it) {cout << *it << " ";}; }
	void print_real_lits() {
		Lit l; 
		cout << "("; 
		for (clause_it it = c.begin(); it != c.end(); ++it) { 
			l = l2rl(*it); 
			cout << l << " ";} cout << ")"; 
	}
	void print_with_watches() {
		//if (verbose < 2) return;
		for (clause_it it = c.begin(); it != c.end(); ++it) {
			cout << *it;
			int j = distance(c.begin(), it); //also could write "int j = i - c.begin();"  : the '-' operator is overloaded to allow such things. but distance is more standard, as it works on all standard containers.
			if (j == lw) cout << "L";
			if (j == rw) cout << "R";
			cout << " ";
		};
	}	
};

class Solver {
	unordered_map<int, Clause> cnf; // clause DB. The key is not continuous because of clause deletion. 
	unsigned int last_permanent_clause; // This will point to the last clause in cnf that all clauses to its left are either original or learned but selected to stay (low lbd / short). 
	vector<int> unaries; 
	trail_t trail;  // assignment stack	
	vector<int> separators; // indices into trail showing increase in dl 	
	vector<int> LitScore; // literal => frequency of this literal (# appearances in all clauses). 
	vector<vector<int> > watches;  // Lit => vector of clause indices into CNF
	vector<VarState> state;  // current assignment
	vector<VarState> prev_state; // for phase-saving: same as state, only that it is not reset to 0 upon backtracking. 
	vector<int> BCP_stack; // vector of assserted literals. 
	vector<int> antecedent; // var => clause index in the cnf vector. For variables that their value was assigned in BCP, this is the clause that gave this variable its value. 
	vector<bool> marked;	// var => seen during analyze()
	vector<int> dlevel; // var => decision level in which this variable was assigned its value. 
	vector<int> conflicts_at_dl; // decision level => # of conflicts under it. Used for local restarts. 
	vector<Lit> assumptions;

	// Used by VAR_DH_MINISAT:	
	map<double, unordered_set<Var>, greater<double>> m_Score2Vars; // 'greater' forces an order from large to small of the keys
	map<double, unordered_set<Var>, greater<double>>::iterator m_Score2Vars_it;
	unordered_set<Var>::iterator m_VarsSameScore_it;
	vector<double>	m_activity; // Var -> activity
	double			m_var_inc;	
	double			m_curr_activity;
	bool			m_should_reset_iterators;

	unsigned int 
		nvars,			// # vars
		nclauses, 		// # original clauses
		nlits,			// # literals = 2*nvars		
		largest_clause_idx, // used for giving an index to a clause in cnf. 
		max_original;
	int
		num_learned,
		num_decisions,
		num_assignments,
		num_restarts,
		dl,				// decision level
		max_dl,			// max dl seen so far
		assumptions_dl,	// == |assumptions cast as decisions | <= |assumptions|. Monotonically decreases. 
		conflicting_clause_idx, // holds the index of the current conflicting clause in cnf[]. -1 if none.				
		last_clause_idx, // location in cnf of the most recent clause (it is not necessarily the last in the array because of the CMTF heuristic)		
		restart_threshold,
		restart_lower,
		restart_upper;
	
	Lit 		asserted_lit;
	vector<int> StatRemovable; // !!
	float restart_multiplier;
	double cla_inc;

	vector<Lit> out_ResponsibleAssumptions;

	// access	
	int get_learned() { return num_learned; }
	void set_nvars(int x) { nvars = x; }
	int get_nvars() { return nvars; }
	void set_nclauses(int x) { nclauses = x; }
	size_t cnf_size() { return cnf.size(); }
	VarState get_state(int x) { return state[x]; }

	// misc.
	void add_to_trail(int x) { trail.push_back(x); }

	void reset(); // initialization that is invoked initially + every restart
	void initialize();
	void reset_iterators(double activity_key = 0.0);
	
	// used by CMTF
	void cmtf_extract(int idx);
	void cmtf_bring_forward(int idx);  // putting clause idx in the end

	// solving	
	SolverState decide();
	short computeLBD(clause_t c);
	void test();
	SolverState BCP();
	int  analyze(const Clause);
	inline int  getVal(Var v);
	inline void add_clause(Clause& c, int l, int r);
	inline void add_unary_clause(Lit l);
	void buildRemovable(); // !!
	bool wouldbeerasednow(Reduce_Heuristic reduceH, int clsidx);// !!
	void reduce();
	inline void assert_lit(Lit l);
	inline void assert_unary(Lit l);
	void m_rescaleScores(double& new_score);
	inline void backtrack(int k);
	void analyze_final(Lit p);
	void restart();
	
	// scores	
	inline void bumpVarScore(int idx);
	inline void bumpLitScore(int lit_idx);
	inline void bumpClauseScore(Clause& c) {c.activity_add(cla_inc);}
	inline void bumpClauseBCPScore(Clause& c) { c.bcp_activity_add(cla_inc); }
	inline void bumpClauseBCPWeightedScore(Clause& c, int weight) { c.bcpWeighted_activity_add(weight); }
	inline void decayClauseActivity() { cla_inc *= 1 / clause_decay; }
	inline void decayVarActivity() { m_var_inc *= 1 / clause_decay; }

public:
	Solver(): 
		nvars(0), nclauses(0), num_learned(0), num_decisions(0), num_assignments(0),
		num_restarts(0), m_var_inc(1.0), max_original(0), assumptions_dl(0), largest_clause_idx(-1),
		cla_inc(1.0),
		restart_threshold(Restart_lower), restart_lower(Restart_lower), 
		restart_upper(Restart_upper), restart_multiplier(Restart_multiplier)	 {};
	
	// service functions
	inline LitState lit_state(Lit l) {
		VarState var_state = state[l2v(l)];
		return var_state == VarState::V_UNASSIGNED ? LitState::L_UNASSIGNED : (Neg(l) && var_state == VarState::V_FALSE || !Neg(l) && var_state == VarState::V_TRUE) ? LitState::L_SAT : LitState::L_UNSAT;
	}
	inline LitState lit_state(Lit l, VarState var_state) {
		return var_state == VarState::V_UNASSIGNED ? LitState::L_UNASSIGNED : (Neg(l) && var_state == VarState::V_FALSE || !Neg(l) && var_state == VarState::V_TRUE) ? LitState::L_SAT : LitState::L_UNSAT;
	}
	void read_cnf(ifstream& in);
	void set_assumptions(vector<Lit> assump);

	void read_assumptions(string filename);
		
	vector<Lit> get_ResponsibleAssumptions() { return out_ResponsibleAssumptions; }

	SolverState _solve();
	SolverState solve();

	
	
	
// debugging
	void print_cnf(){
		if (verbose < 2) return; 
		for (unordered_map<int, Clause>::iterator it = cnf.begin(); it != cnf.end(); ++it) {
			it ->second.print_with_watches(); 
			cout << endl;
		}
	} 

	void print_ordered_cnf() {
		if (verbose < 2) return;
		cout << "ordered cnf, reverse order: " << endl;
		for (int i = last_clause_idx; i >= 0; i = cnf[i].get_prev())
			cnf[i].print_real_lits();
		cout << endl;
	};

	void print_real_cnf() {
		if (verbose < 2) return; 
		for (unordered_map<int, Clause>::iterator it = cnf.begin(); it != cnf.end(); ++it) {
			it->second.print_real_lits(); 
			cout << endl;
		}
	} 

	void print_state(const char *file_name) {
		ofstream out;
		out.open(file_name);		
		out << "State: "; 
		for (vector<VarState>::iterator it = state.begin() + 1; it != state.end(); ++it) {
			char sign = (*it) == VarState::V_FALSE ? -1 : (*it) == VarState::V_TRUE ? 1 : 0;
			out << sign * (it - state.begin()) << " "; out << endl;
		}
	}	

	void print_state() {
		cout << "State: "; 
		for (vector<VarState>::iterator it = state.begin() + 1; it != state.end(); ++it) {
			char sign = (*it) == VarState::V_FALSE ? -1 : (*it) == VarState::V_TRUE ? 1 : 0;
			cout << sign * (it - state.begin()) << " "; cout << endl;
		}
	}	
	
	void print_watches() {
		if (verbose < 2) return;
		for (vector<vector<int> >::iterator it = watches.begin() + 1; it != watches.end(); ++it) {
			cout << distance(watches.begin(), it) << ": ";
			for (vector<int>::iterator it_c = (*it).begin(); it_c != (*it).end(); ++it_c) {
				cnf[*it_c].print();
				cout << "; ";
			}
			cout << endl;
		}
	};


	void print_stats() {cout << endl << "Statistics: " << endl << "===================" << endl << 
		"### Restarts:\t\t" << num_restarts << endl <<
		"### Learned-clauses:\t" << num_learned << endl <<
		"### Decisions:\t\t" << num_decisions << endl <<
		"### Implications:\t" << num_assignments - num_decisions << endl <<
		"### Time:\t\t" << cpuTime() - begin_time << endl;
	}
	
	void validate_assignment();
};


