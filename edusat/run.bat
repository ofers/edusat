for %%f in (test\sat\*.cnf) do (
..\release\edusat %1 %2 %3 %4 %%f > tmp.out
grep SAT tmp.out
grep Assignment tmp.out
grep Time tmp.out
)
for %%f in (test\unsat\*.cnf) do (
..\release\edusat %1 %2 %3 %4 %%f > tmp.out
grep UNSAT tmp.out
grep Time tmp.out
)