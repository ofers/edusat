EduSAT - a basic SAT solver. (c) ofer strichman
===============================================

Type edusat -h for options. 

Clarifications: 

1. literal indexing: 
Internally, literal i is represented with the integer 2i, and literal -i with the integer 2i-1.
e.g. the clause (3 -4) is represented by (6 7)
See the functions v2l() and l2v() to understand the conversions. 
The Neg(Lit l) function changes the sign of a literal. 

2. 'BCP_stack' contains the negation of the asserted literals that were not yet processed in BCP. 
watches[lit] contain a list of clauses that contain 'lit'. 
So if, e.g., we assert literal '8', we insert to BCP_stack the literal '7' (== Neg(8)), and then in BCP we traverse the claues in watches[7]. 
