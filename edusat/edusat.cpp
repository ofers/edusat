// TODO: 
// 1. lbd
// 2. learning-rate decision heuristic (vijay's sat'16)
// 3. deletion strategy
// 4. preprocessing
// 5. conf. clause shrinking
// 6. seperate treatment of binary clauses 
// 7. Watchers with blocking clauses and possibly more literals (p. stucky's cache awarness paper)

#include "edusat.h"
#include <stack>
#include <iomanip>
#include <numeric>

Solver S;

using namespace std;

inline bool verbose_now() {
	return false;
}

void Abort(string s, int i) {
	cout << endl << "Abort: ";
	switch (i) {
	case 1: cout << "(input error)" << endl; break;
	case 2: cout << "command line arguments error" << endl; break;
	case 3: break;
	default: cout << "(exit code " << i << ")" << endl; break;
	}
	cout << s << endl;
	//	S.print_scores(); 
	//	S.print_ordered_cnf();
	exit(i);
}


/******************  Reading the CNF ******************************/
#pragma region readCNF
void skipLine(ifstream& in) {
	for (;;) {
		//if (in.get() == EOF || in.get() == '\0') return;
		if (in.get() == '\n') { return; }
	}
}

static void skipWhitespace(ifstream& in, char&c) {
	c = in.get();
	while ((c >= 9 && c <= 13) || c == 32)
		c = in.get();
}

static int parseInt(ifstream& in) {
	int     val = 0;
	bool    neg = false;
	char c;
	skipWhitespace(in, c);
	if (c == '-') neg = true, c = in.get();
	if (c < '0' || c > '9') cout << c, Abort("Unexpected char in input", 1);
	while (c >= '0' && c <= '9')
		val = val * 10 + (c - '0'),
		c = in.get();
	return neg ? -val : val;
}

void Solver::read_cnf(ifstream& in) {
	int i;
	unsigned int vars, clauses, unary = 0;
	set<Lit> s;
	Clause c;


	while (in.peek() == 'c') skipLine(in);

	if (!match(in, "p cnf")) Abort("Expecting `p cnf' in the beginning of the input file", 1);
	in >> vars; // since vars is int, it reads int from the stream.
	in >> clauses;
	if (!vars || !clauses) Abort("Expecting non-zero variables and clauses", 1);
	cout << "vars: " << vars << " clauses: " << clauses << endl;

	set_nvars(vars);
	set_nclauses(clauses);
	initialize();

	while (in.good() && in.peek() != EOF) {
		i = parseInt(in);
		if (i == 0) {
			c.cl().resize(s.size());
			copy(s.begin(), s.end(), c.cl().begin());
			switch (c.size()) {
			case 0: {
				stringstream num;  // this allows to convert int to string
				num << cnf_size() + 1; // converting int to string.
				Abort("Empty clause not allowed in input formula (clause " + num.str() + ")", 1); // concatenating strings
			}
			case 1: {
				Lit l = c.cl()[0];
				// checking if we have conflicting unaries. Sufficiently rare to check it here rather than 
				// add a check in BCP. 
				if (state[l2v(l)] != VarState::V_UNASSIGNED)
					if (Neg(l) != (state[l2v(l)] == VarState::V_FALSE)) {
						S.print_stats();
						Abort("UNSAT (conflicting unaries for var " + to_string(l2v(l)) +")", 0);
					}
				assert_unary(l);
				BCP_stack.push_back(opposite(l));
				add_unary_clause(l);
				break; // unary clause. Note we do not add it as a clause. 
			}
			default: add_clause(c, 0, 1);
			}
			c.reset();
			s.clear();
			continue;
		}
		if (Abs(i) > vars) Abort("Literal index larger than declared on the first line", 1);
		if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) bumpVarScore(abs(i));
		i = v2l(i);		
		if (ValDecHeuristic == VAL_DEC_HEURISTIC::LITSCORE) bumpLitScore(i);
		s.insert(i);
	}
	last_clause_idx = max_original = cnf_size() - 1;
	last_permanent_clause = last_clause_idx; //cnf.find(last_clause_idx);
	//Assert(last_permanent_clause != cnf.end());
	if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) reset_iterators();
	cout << "Read " << cnf_size() << " clauses in " << cpuTime() - begin_time << " secs." << endl << "Solving..." << endl;
}

#pragma endregion readCNF

/******************  Solving ******************************/
#pragma region solving
void Solver::reset() { // invoked initially + every restart
	dl = 0;
	max_dl = 0;// assumptions_dl;
	conflicting_clause_idx = -1;	
	separators.push_back(0); // we want separators[1] to match dl=1. separators[0] is not used.
	conflicts_at_dl.push_back(0);
}


inline void Solver::reset_iterators(double where) {
	m_Score2Vars_it = (where == 0) ? m_Score2Vars.begin() : m_Score2Vars.lower_bound(where);
	Assert(m_Score2Vars_it != m_Score2Vars.end());
	m_VarsSameScore_it = m_Score2Vars_it->second.begin();
	m_should_reset_iterators = false;
}

void Solver::initialize() {	
	
	state.resize(nvars + 1, VarState::V_UNASSIGNED);
	prev_state.resize(nvars + 1, VarState::V_FALSE); // we set initial assignment with phase-saving to false. 
	antecedent.resize(nvars + 1, -1);	
	marked.resize(nvars+1);
	dlevel.resize(nvars+1);
	
	nlits = 2 * nvars;
	watches.resize(nlits + 1);
	LitScore.resize(nlits + 1);
	//initialize scores 	
	m_activity.resize(nvars + 1);	
	m_curr_activity = 0.0f;
	for (unsigned int v = 0; v <= nvars; ++v) {			
		m_activity[v] = 0;		
	}
	reset();
}

inline void Solver::assert_lit(Lit l) {
	trail.push_back(l);
	int var = l2v(l);
	if (Neg(l)) prev_state[var] = state[var] = VarState::V_FALSE; else prev_state[var] = state[var] = VarState::V_TRUE;
	dlevel[var] = dl;
	++num_assignments;
	if (verbose > 1) cout << "v" << var << "(lit " << l << "):" << static_cast<int>(state[var]) << "@" << dl << endl;
}

inline void Solver::assert_unary(Lit l) {		// the difference is that we do not push unaries to the trail, and also force dlevel = 0
	int var = l2v(l);
	if (Neg(l)) state[var] = VarState::V_FALSE; else state[var] = VarState::V_TRUE;
	dlevel[var] = 0;
	++num_assignments;
	if (verbose > 1) cout << "v" << var << "(lit " << l << "):" << static_cast<int>(state[var]) << "@" << 0 << endl;
}

void Solver::m_rescaleScores(double& new_score) {
	if (verbose_now()) cout << "Rescale" << endl;
	new_score /= Rescale_threshold;
	for (unsigned int i = 1; i <= nvars; i++)
		m_activity[i] /= Rescale_threshold;
	m_var_inc /= Rescale_threshold;
	// rebuilding the scaled-down m_Score2Vars.
	map<double, unordered_set<Var>, greater<double>> tmp_map;
	double prev_score = 0.0f;
	for (auto m : m_Score2Vars) {
		double scaled_score = m.first / Rescale_threshold;
		if (scaled_score == prev_score) // This can happen due to rounding
			tmp_map[scaled_score].insert(m_Score2Vars[m.first].begin(), m_Score2Vars[m.first].end());
		else
			tmp_map[scaled_score] = m_Score2Vars[m.first];
		prev_score = scaled_score;
	}
	tmp_map.swap(m_Score2Vars);
}

void Solver::bumpVarScore(int var_idx) {
	if (verbose_now()) cout << "bumpVarScore" << endl;
	double new_score;
	double score = m_activity[var_idx];		

	if (score > 0) {
		Assert(m_Score2Vars.find(score) != m_Score2Vars.end());
		m_Score2Vars[score].erase(var_idx);
		if (m_Score2Vars[score].size() == 0) m_Score2Vars.erase(score);
	}
	new_score = score + m_var_inc;
	m_activity[var_idx] = new_score;

	// Rescaling, to avoid overflows; 
	if (new_score > Rescale_threshold) {
		m_rescaleScores(new_score);
	}

	if (m_Score2Vars.find(new_score) != m_Score2Vars.end())
		m_Score2Vars[new_score].insert(var_idx);
	else
		m_Score2Vars[new_score] = unordered_set<int>({ var_idx });
}

void Solver::bumpLitScore(int lit_idx) {
	LitScore[lit_idx]++;
}

void Solver::add_clause(Clause& c, int l, int r) {	
	Assert(c.size() > 1) ;
	c.lw_set(l);
	c.rw_set(r);
	int loc = ++largest_clause_idx; //static_cast<int>(cnf.size());  // the first is in location 0 in cnf
	if (VarDecHeuristic == VAR_DEC_HEURISTIC::CMTF) {
		c.prev_set(loc - 1);  	
		if (!max_original && (loc - 1 >= 0)) { // only originals ('next' for num_learned clauses will be updated in cmtf_move_forward)
			cnf[loc - 1].next_set(loc);
		}	
	}
	
	
	watches[c.lit(l)].push_back(loc); 
	watches[c.lit(r)].push_back(loc);
	cnf[loc]=c;	
}

void Solver::add_unary_clause(Lit l) {		
	unaries.push_back(l);
}

void Solver::buildRemovable() {
	
	if (verbose >= 1) cout << "reduce" << endl;
	for (unsigned int i = last_permanent_clause; i <= largest_clause_idx; ++i) {

		Clause cl = cnf[i];
		if (cl.get_lbd() <= 3) continue;
		StatRemovable.push_back(i);
	}
}

bool Solver::wouldbeerasednow(Reduce_Heuristic reduceH, int clsidx) {
	switch (reduceH)
	{
	case Reduce_Heuristic::LBD3_ClauseActivity:
		sort(StatRemovable.begin(), StatRemovable.end(),
			[&](int a, int b) {return cnf[a].get_activity() < cnf[b].get_activity(); });
		break;
	case Reduce_Heuristic::LBD3_BCPActivity:
		sort(StatRemovable.begin(), StatRemovable.end(),
			[&](int a, int b) {return cnf[a].get_bcp_activity() < cnf[b].get_bcp_activity(); });
		break;
	case Reduce_Heuristic::LBD3_BCPWActivity:
		sort(StatRemovable.begin(), StatRemovable.end(),
			[&](int a, int b) {return cnf[a].get_bcpWeighted_activity() < cnf[b].get_bcpWeighted_activity(); });
		break;
	}
	int half = StatRemovable.size() / 2;
	return (find(StatRemovable.begin(), StatRemovable.begin() + half, clsidx) != StatRemovable.begin() + half);

}





void Solver::reduce() {
	vector<int> removable;
	if (verbose >= 1) cout << "reduce" << endl;
	for (unsigned int i = last_permanent_clause; i <= largest_clause_idx; ++i) {
		
		// check if the clause has been erased. 
		if (cnf.count(i) == 0) continue;
		
		Clause cl = cnf[i];
		if (cl.get_lbd() <= 3) continue;
		
		// check if the clause is locked (i.e., an antecedent)
		Lit l = cl.get_lw_lit();
		if (antecedent[l2v(l)] == i) continue;
		l = cl.get_rw_lit();
		if (antecedent[l2v(l)] == i) continue;				

		removable.push_back(i);		
	}

	// removing the less-active half. 
	// The '&' in the lambda means that all variables are accessible by reference.
	switch (ReduceHeuristic) {
	case Reduce_Heuristic::LBD3_ClauseActivity:
		sort(removable.begin(), removable.end(),
			[&](int a, int b) {return cnf[a].get_activity() < cnf[b].get_activity(); });
		break;
	case Reduce_Heuristic::LBD3_BCPActivity:
		sort(removable.begin(), removable.end(),
			[&](int a, int b) {return cnf[a].get_bcp_activity() < cnf[b].get_bcp_activity(); });
		break;
	case Reduce_Heuristic::LBD3_BCPWActivity: 
		sort(removable.begin(), removable.end(),
			[&](int a, int b) {return cnf[a].get_bcpWeighted_activity() < cnf[b].get_bcpWeighted_activity(); });
		break;
	}
	int half = removable.size() / 2;

	unordered_set<Lit> rebuildWatches;	
	for (int i = 0; i < half; ++i) {				
		Clause cl = cnf[removable[i]];		
		rebuildWatches.insert(cl.get_lw_lit());
		rebuildWatches.insert(cl.get_rw_lit());
		cnf.erase(removable[i]);
	}
	
	// now clean watch lists: 
	for (auto l : rebuildWatches) {
		unsigned int i, j;
		for (i = 0, j = 0; i < watches[l].size(); ++i) {
			if (cnf.count(watches[l][i]) == 1) watches[l][j++] = watches[l][i];
		}
		Assert(j < watches[l].size());
		watches[l].resize(j);
	}
}



int Solver :: getVal(Var v) {
	switch (ValDecHeuristic) {
	case VAL_DEC_HEURISTIC::PHASESAVING: {
		VarState saved_phase = prev_state[v];		
		switch (saved_phase) {
		case VarState::V_FALSE:	return v2l(-v);
		case VarState::V_TRUE: return v2l(v);
		default: Assert(0);
		}
	}
	case VAL_DEC_HEURISTIC::LITSCORE:
	{
		int litp = v2l(v), litn = v2l(-v);
		int pScore = LitScore[litp], nScore = LitScore[litn];
		return pScore > nScore ? litp : litn;
	}
	default: Assert(0);
	}	
	return 0;
}

SolverState Solver::decide(){
	if (verbose_now()) cout << "decide" << endl;
	Lit best_lit = 0;
	int max_score = 0;
	Var bestVar = 0;
	if (dl < assumptions_dl) {
		for (vector<Lit>::iterator it = assumptions.begin() + dl; it < assumptions.end(); ++it) {
			switch (lit_state(*it)) {
			case LitState::L_UNSAT:
				out_ResponsibleAssumptions.push_back(*it); 
				analyze_final(*it);  
				return SolverState::UNSAT;
			case LitState::L_UNASSIGNED: best_lit = *it;
				goto Apply_decision;
			}
		}		
		assumptions_dl = dl; // we get here only when the rest of the assumptions are already determined by previous assumptions. 
	}
	
	

	switch (VarDecHeuristic) {
	case  VAR_DEC_HEURISTIC::CMTF:
		for (int it = last_clause_idx; !best_lit && it >= 0; it = cnf.at(it).get_prev()) {	// go over clauses 
			for (vector<Lit>::reverse_iterator it_c = cnf.at(it).cl().rbegin(); it_c != cnf.at(it).cl().rend(); ++it_c) { // go over literals in the clause								
				Var v = l2v(*it_c);
				LitState res = lit_state(*it_c, state[v]);
				if (res == LitState::L_SAT)  break; // clause is satisfied. Skip to next one.
				if (res == LitState::L_UNASSIGNED) { 
					best_lit = getVal(v);
					goto Apply_decision;
				}
			}
		}
		break;
	case  VAR_DEC_HEURISTIC::MINISAT: {
		// m_Score2Vars_r_it and m_VarsSameScore_it are fields. 
		// When we get here they are the location where we need to start looking. 		
		if (m_should_reset_iterators) reset_iterators(m_curr_activity);
		Var v = 0;
		int cnt = 0;
		if (m_Score2Vars_it == m_Score2Vars.end()) break;
		while (true) { // scores from high to low
			while (m_VarsSameScore_it != m_Score2Vars_it->second.end()) {
				v = *m_VarsSameScore_it;
				++m_VarsSameScore_it;
				++cnt;
				if (state[v] == VarState::V_UNASSIGNED) { // found a var to assign
					m_curr_activity = m_Score2Vars_it->first;
					assert(m_curr_activity == m_activity[v]);
					best_lit = getVal(v);					
					goto Apply_decision;
				}
			}
			++m_Score2Vars_it;
			if (m_Score2Vars_it == m_Score2Vars.end()) break;
			m_VarsSameScore_it = m_Score2Vars_it->second.begin();
		}
		break;
	}
	default: Assert(0);
	}	
		
	assert(!best_lit);
	S.print_state(Assignment_file);
	return SolverState::SAT;


Apply_decision:	
	dl++; // increase decision level
	if (dl > max_dl) {
		max_dl = dl;
		separators.push_back(trail.size());
		conflicts_at_dl.push_back(num_learned);
	}
	else {
		separators[dl] = trail.size();
		conflicts_at_dl[dl] = num_learned;
	}
	
	assert_lit(best_lit);
	++num_decisions;
	BCP_stack.push_back(opposite(best_lit));
	if (verbose > 1) cout << "BCP_stack <- " << opposite(best_lit) << endl;
	return SolverState::UNDEF;
}

inline ClauseState Clause::next_not_false(bool is_left_watch, Lit other_watch, bool binary, short& loc) {  // lit is the currently watched literal
	// if (verbose_now()) cout << "next_not_false" << endl;
	if (verbose > 1) {
		cout << "searching next-not-false in: "; print_with_watches();
		Lit lit = is_left_watch ? c[lw]: c[rw];
		cout << endl << "with literal " << lit << endl;
	}	
	
	if (!binary)
		for (vector<int>::iterator it = c.begin(); it != c.end(); ++it) {
			LitState LitState = S.lit_state(*it);
			if (LitState != LitState::L_UNSAT && *it != other_watch) { // found another watch_lit
				loc = distance(c.begin(), it);
				if (is_left_watch) lw = loc;    // if literal was the left one 
				else rw = loc;				
				return ClauseState::C_UNDEF;
			}
		}
	switch (S.lit_state(other_watch)) {
	case LitState::L_UNSAT: // conflict
		if (verbose > 1) { print(); cout << " is conflicting" << endl; }
		return ClauseState::C_UNSAT;
	case LitState::L_UNASSIGNED: return ClauseState::C_UNIT; // unit clause. Should assert the other watch_lit.	
	case LitState::L_SAT: return ClauseState::C_SAT; // other literal is satisfied. 
	default: Assert(0); return ClauseState::C_UNDEF; // just to supress warning. 
	}
}

short Solver::computeLBD(clause_t c)
{
	unordered_set<int> levels;
	for (Lit l : c) {
		int level = dlevel[l2v(l)];
		levels.insert(level);
	}
	return (short)levels.size();
}

void Solver::test() { // tests that each clause is watched twice. 	
	for (unordered_map<int,Clause>::iterator cl_it = cnf.begin(); cl_it != cnf.end(); ++cl_it) {
		int idx = cl_it->first;
		Clause c = cl_it->second;
		bool found = false;
		for (int zo = 0; zo <= 1; ++zo) {
			for (vector<int>::iterator it = watches[c.cl()[zo]].begin(); !found && it != watches[c.cl()[zo]].end(); ++it) {				
				if (*it == idx) {
					found = true;
					break;
				}
			}
		}
		if (!found) {
			cout << "idx = " << idx << endl;
			c.print();
			cout << endl;
			cout << c.size();
		}
		Assert(found);
	}
}

SolverState Solver::BCP() {
	if (verbose_now()) cout << "BCP" << endl;
	unsigned int counter = 0;
	map<int, pair<int, int> > implicationCount; // stack-depth |-> <clause-index, counter>. 
	// The idea: Suppose that reason(x) = c (c being the clause index here), and x is the
	// literal we take out from BCP_stack, suppose at depth i of the stack. 
	// We maintain a pair <c, counter>, where counter is the running counter value.
	// We record the counter on the way up (i.e., removing x from BCP_stack) and on the 
	// way down (going from level i to i-1 again). The difference in the values of 'counter' is
	// the number of implications that were triggered by x. So this is another measure of 
	// the importance of clause c, to be used when deleting. 
	while(!BCP_stack.empty()) {
		Lit lit = BCP_stack.back();
		Assert(lit_state(lit) == LitState::L_UNSAT);
		if (verbose > 1) cout << "BCP_stack -> " << lit << endl;
		
		// weighted score for clauses
		//if (ReduceHeuristic == Reduce_Heuristic::LBD3_BCPWActivity) // !!
		{
			int stackDepth = BCP_stack.size();
			if (implicationCount.count(stackDepth) == 1) { // on the way down
				int ant = implicationCount[stackDepth].first;
				if (ant > 0) { // nothing to do if it was an assignment
					unsigned int implied = counter - implicationCount[stackDepth].second;
					bumpClauseBCPWeightedScore(cnf[ant], implied);					
					implicationCount.erase(stackDepth);
				}
			}
			else { // on the way up
				int ant = antecedent[l2v(lit)];
				pair<int, int> p(ant, counter);
				implicationCount[stackDepth] = p;
			}		
			counter++;
		}

		BCP_stack.pop_back();
		
		vector<int> new_watch_list; // The original watch list minus those clauses that changed a watch. The order is maintained. 
		int new_watch_list_idx = watches[lit].size() - 1; // Since we are traversing the watch_list backwards, this index goes down.
		new_watch_list.resize(watches[lit].size());
		for (vector<int>::reverse_iterator it = watches[lit].rbegin(); it != watches[lit].rend() && conflicting_clause_idx < 0; ++it) {
			// Assert(cnf.count(*it) == 1); 			
			Clause& c = cnf[*it];
			Lit l_watch = c.get_lw_lit(), 
				r_watch = c.get_rw_lit();			
			bool binary = c.size() == 2;
			bool is_left_watch = (l_watch == lit);
			Lit other_watch = is_left_watch? r_watch: l_watch;
			short NewWatchLocation;
			ClauseState res = c.next_not_false(is_left_watch, other_watch, binary, NewWatchLocation);
			if (res != ClauseState::C_UNDEF) new_watch_list[new_watch_list_idx--] = *it; //in all cases but the move-watch_lit case we leave watch_lit where it is
			switch (res) {
			case ClauseState::C_UNSAT: { // conflict				
				if (verbose > 1) print_state();
				if (dl == 0) return SolverState::UNSAT;
				if (dl <= assumptions_dl) {
					analyze_final(lit);
					return SolverState::UNSAT;
				}
				conflicting_clause_idx = *it;  // this will also break the loop
				BCP_stack.clear();
				 int dist = distance(it, watches[lit].rend()) - 1; // # of entries in watches[lit] that were not yet processed when we hit this conflict. 
				// Copying the remaining watched clauses:
				for (int i = dist - 1; i >= 0; i--) {
					new_watch_list[new_watch_list_idx--] = watches[lit][i];
				}
				break;
			}
			case ClauseState::C_SAT: break; // nothing to do when clause has a satisfied literal.
			case ClauseState::C_UNIT: { // new implication				
				if (verbose > 1) cout << "propagating: ";
				assert_lit(other_watch);
				BCP_stack.push_back(opposite(other_watch));
				antecedent[l2v(other_watch)] = *it;
				//if (ReduceHeuristic == Reduce_Heuristic::LBD3_BCPActivity) // !!
				bumpClauseBCPScore(c);
				if (verbose > 1) cout << "BCP_stack <- " << opposite(other_watch) << endl;
				break;
			}
			default: // replacing watch_lit
				Assert(NewWatchLocation < static_cast<int>(c.size()));
				int new_lit = c.lit(NewWatchLocation);
				watches[new_lit].push_back(*it);
				if (verbose > 1) { c.print(); cout << "now watched by " << new_lit << endl;}				
			}
		}
		// resetting the list of clauses watched by this literal.
		watches[lit].clear();
		new_watch_list_idx++; // just because of the redundant '--' at the end. 		
		watches[lit].insert(watches[lit].begin(), new_watch_list.begin() + new_watch_list_idx, new_watch_list.end());

		//print_watches();
		if (conflicting_clause_idx >= 0) return SolverState::CONFLICT;
		new_watch_list.clear();
	}
	return SolverState::UNDEF;
}

// putting clause idx in the end
void Solver::cmtf_bring_forward(int idx) { // TODO: as of clause deletion, check the behavior of last_clause_idx
	if (idx == last_clause_idx) return;	
	Clause& c = cnf.at(idx);
	cnf.at(last_clause_idx).next_set(idx);
	c.prev_set(last_clause_idx);
	c.next_set(largest_clause_idx);
	last_clause_idx = idx;
}

// taking clause idx out of its current_clause location. Should be followed by cmtf_bring_forward 
void Solver::cmtf_extract(int idx) { 
	if (idx == last_clause_idx) return;	
	Clause& c = cnf.at(idx);
	unsigned int next = c.get_next();	
	Assert(next <= largest_clause_idx);
	int prev = c.get_prev();
	cnf.at(next).prev_set(prev);
	if (prev >=0) cnf.at(prev).next_set(next);
	//print_ordered_cnf();
	//	check_cyclicity();
}

/*******************************************************************************************************************
name: analyze
input:	1) conflicting clause
		2) dlevel
		3) marked
		
assumes: 1) no clause should have the same literal twice. To guarantee this we read through a set in read_cnf. 
            Wihtout this assumption it may loop forever because we may remove only one copy of the pivot.

This is Alg. 1 from "HaifaSat: a SAT solver based on an Abstraction/Refinement model" 
********************************************************************************************************************/

int Solver::analyze(const Clause conflicting) {
	if (verbose_now()) cout << "analyze" << endl;
	Clause	current_clause = conflicting, 
			new_clause;
	int resolve_num = 0,
		bktrk = 0, 
		watch_lit = 0, // points to what literal in the learnt clause should be watched, other than the asserting one
		antecedents_idx = 0, 
		cmtf_forward_counter = 0;

	Lit u;
	Var v;
	trail_t::reverse_iterator t_it = trail.rbegin();
	do {
		for (clause_it it = current_clause.cl().begin(); it != current_clause.cl().end(); ++it) {
			Lit lit = *it;
			v = l2v(lit);
			if (!marked[v]) {
				marked[v] = true;
				if (dlevel[v] == dl) ++resolve_num;
				else { // literals from previos decision levels (roots) are entered to the learned clause.
					new_clause.insert(lit);
					if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) bumpVarScore(v);
					if (ValDecHeuristic == VAL_DEC_HEURISTIC::LITSCORE) bumpLitScore(lit);
					int c_dl = dlevel[v];
					if (c_dl > bktrk) {
						bktrk = c_dl;
						watch_lit = new_clause.size() - 1;
					}
				}
			}
		}
		
		while (t_it != trail.rend()) {
			u = *t_it;
			v = l2v(u);
			++t_it;
			if (marked[v]) break;
		}
		marked[v] = false;
		--resolve_num;
		if(!resolve_num) continue; 
		unsigned int ant = antecedent[v];		
		current_clause = cnf[ant]; 
		if (ant > last_permanent_clause) bumpClauseScore(cnf[ant]);			
		
		current_clause.cl().erase(find(current_clause.cl().begin(), current_clause.cl().end(), u));
		if (VarDecHeuristic == VAR_DEC_HEURISTIC::CMTF && cmtf_forward_counter++ < Max_bring_forward) {
			cmtf_extract(ant); 
			cmtf_bring_forward(ant);
		}
	}	while (resolve_num > 0);
	for (clause_it it = new_clause.cl().begin(); it != new_clause.cl().end(); ++it) 
		marked[l2v(*it)] = false;
	Lit opp_u = opposite(u);
	new_clause.cl().push_back(opp_u);		
	if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) 
		decayVarActivity(); // increasing importance of participating variables.
	decayClauseActivity(); // increasing importance of participating clauses.

	++num_learned;
	

	asserted_lit = opp_u;
	if (new_clause.size() == 1) { // unary clause	
		BCP_stack.push_back(u); 
		add_unary_clause(opp_u);
		if (verbose > 1) cout << "BCP_stack <- " << u << endl;
	}
	else {
		BCP_stack.push_back(u); // this way after backtracking we will handle the new clause.
		short lbd = computeLBD(new_clause.cl());
		new_clause.lbd_set(lbd);
		add_clause(new_clause, watch_lit, new_clause.size() - 1);
		//cout << "added conflict" << endl;
		if (VarDecHeuristic == VAR_DEC_HEURISTIC::CMTF) cmtf_bring_forward(largest_clause_idx); // this takes care of the prev/next in cnf for new_clause.
		if (verbose > 1) cout << "BCP_stack <- " << new_clause.cl()[watch_lit] << endl;
	}
	

	if (verbose_now()) {	
		cout << "Learned clause #" << largest_clause_idx + unaries.size() << ". ";
		new_clause.print(); 
		cout << endl;
		cout << " learnt clauses:  " << num_learned;				
		cout << " Backtracking to level " << bktrk << endl;
	}

	if (verbose >= 1 && !(num_learned % 1000)) {
		cout << "Learned: "<< num_learned <<" clauses. Active: " << cnf.size() - max_original << endl;
	}	
	return bktrk; 
}

void Solver::backtrack(int k) {
	if (verbose_now()) cout << "backtrack" << endl;
	if (k > 0 && (num_learned - conflicts_at_dl[k] > restart_threshold)) {	// "local restart"	
		restart(); 		
		return;
	}
	static int counter = 0;
		
	for (trail_t::iterator it = trail.begin() + separators[k+1]; it != trail.end(); ++it) { // erasing from k+1
		Var v = l2v(*it);
		if (dlevel[v]) { // we need the condition because of learnt unary clauses. In that case we enforce an assignment with dlevel = 0.
			state[v] = VarState::V_UNASSIGNED;
			if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) m_curr_activity = max(m_curr_activity, m_activity[v]);
		}
	}
	if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) m_should_reset_iterators = true;
	if (verbose > 1) print_state();
	trail.erase(trail.begin() + separators[k+1], trail.end());
	dl = k;
	if (k == 0) assert_unary(asserted_lit);
	else assert_lit(asserted_lit);
	antecedent[l2v(asserted_lit)] = largest_clause_idx;
	conflicting_clause_idx = -1;
}

void Solver::validate_assignment() {
	for (unsigned int i = 1; i <= nvars; ++i) if (state[i] == VarState::V_UNASSIGNED) {
		cout << "Unassigned var: " + to_string(i) << endl; // This is supposed to happen only if the variable does not appear in any clause
	}
	

	for (unordered_map<int, Clause>::iterator it = cnf.begin(); it != cnf.end(); ++it) {
		int found = 0;
		Clause cl = it->second;
		for(clause_it it_c = cl.cl().begin(); it_c != cl.cl().end() && !found; ++it_c) 
			if (lit_state(*it_c) == LitState::L_SAT) found = 1;
		if (!found) {
			cout << "fail on clause: "; 
			cl.print();
			cout << endl;
			for (clause_it it_c = cl.cl().begin(); it_c != cl.cl().end() && !found; ++it_c)
				cout << *it_c << " (" << (int) lit_state(*it_c) << ") ";
			cout << endl;
			Abort("Assignment validation failed", 3);
		}
	}
	for (vector<Lit>::iterator it = unaries.begin(); it != unaries.end(); ++it) {
		if (lit_state(*it) != LitState::L_SAT) 
			Abort("Assignment validation failed (unaries)", 3);
	}
	cout << "Assignment validated" << endl;
}

void Solver::restart() {	
	if (verbose >= 1) cout << "restart" << endl;
	restart_threshold = static_cast<int>(restart_threshold * restart_multiplier);
	if (restart_threshold > restart_upper) {
		restart_threshold = restart_lower;
		restart_upper = static_cast<int>(restart_upper  * restart_multiplier);
	}
	if (verbose >=1) cout << "restart: new threshold = " << restart_threshold <<", upper = " << restart_upper << endl;
	++num_restarts;
	for (unsigned int i = 1; i <= nvars; ++i) 
		if (dlevel[i] > 0) {
			state[i] = VarState::V_UNASSIGNED;
			dlevel[i] = 0;
		}	
	BCP_stack.clear();
	trail.clear();
	separators.clear(); // resize(assumptions_dl); we can resize, but in reset we push(0) as if it is level 0.
	conflicts_at_dl.clear(); //  resize(assumptions_dl);
	if (VarDecHeuristic == VAR_DEC_HEURISTIC::MINISAT) {
		m_curr_activity = 0; // The activity does not really become 0. When it is reset in decide() it becomes the largets activity. 
		m_should_reset_iterators = true;
	}
	reset();
}

void Solver::analyze_final(Lit p) {
	vector<bool> seen;
	if (verbose >= 1) cout << "analyze_final" << endl;
	seen.resize(nvars + 1);
	if (dl > 0) {
		seen[l2v(p)] = true;
		for (int i = trail.size() - 1; i >= separators[1]; i--) {
			Var x = l2v(trail[i]);
			if (seen[x]) {
				if (antecedent[x] == -1) {
					out_ResponsibleAssumptions.push_back(trail[i]);
				}
				else {
					Clause& c = cnf[antecedent[x]];
					for (unsigned int j = 0; j < c.size(); j++)
						if ((dlevel[l2v(c.cl()[j])]) > 0)
						{
							seen[l2v(c.cl()[j])] = true;
						}
				}
			}
		}
	}
	cout << "Assumptions causing the conflict: ";
	for (auto l : out_ResponsibleAssumptions) cout << l2rl(l) << " ";
}

SolverState Solver::solve() { // wrapper for incremental SAT. 
	out_ResponsibleAssumptions.clear();
	SolverState res = _solve(); 	
	Assert(res == SolverState::SAT || res == SolverState::UNSAT || res == SolverState::TIMEOUT);
	S.print_stats();
	switch (res) {
	case SolverState::SAT: {
		S.validate_assignment();
		string str = "solution in ",
			str1 = Assignment_file;
		cout << str + str1 << endl;
		cout << "SAT" << endl;
		break;
	}
	case SolverState::UNSAT: 
		cout << "UNSAT" << endl;
		break;
	case SolverState::TIMEOUT: 
		cout << "TIMEOUT" << endl;
		return res;
	}
	restart(); // we restart just in case Solve will be called again.
	return res;
}

SolverState Solver::_solve() {
	SolverState res;
	ofstream myfile;
	myfile.open("c:\\temp\\consistency.csv");
	myfile << "conflicts,clause,heuristic,erased" << endl;
	vector<bool> reduced1, reduced2, reduced3;
	vector<int> revived1 (100), revived2(100), revived3(100); // # of times disagree with the initial decision to reduce/not to reduce. 
	
	while (true) {
		if (timeout > 0 && cpuTime() - begin_time > timeout) return SolverState::TIMEOUT;
		while (true) {
			res = BCP();
			if (res == SolverState::UNSAT) return res;
			if (res == SolverState::CONFLICT)
				backtrack(analyze(cnf[conflicting_clause_idx]));
			else break;
			// !!
			if (num_learned == ReduceClauses) buildRemovable(); // !!
			if (num_learned >= ReduceClauses && ((num_learned - ReduceClauses) % 100 == 0)) {
				for (int i = max_original + 1; i < max_original + 1000; i += 10) {
					int j = 0;
					bool
						h1 = wouldbeerasednow(Reduce_Heuristic::LBD3_ClauseActivity, i),
						h2 = wouldbeerasednow(Reduce_Heuristic::LBD3_BCPActivity, i),
						h3 = wouldbeerasednow(Reduce_Heuristic::LBD3_BCPWActivity, i);
					if (num_learned == ReduceClauses) { // supposedly where we reduced
						reduced1.push_back(h1); reduced2.push_back(h2); reduced3.push_back(h3);
					}
					else {						
						if (h1 != reduced1[j]) revived1[j]++;
						if (h2 != reduced2[j]) revived2[j]++;
						if (h3 != reduced3[j]) revived3[j]++;
						j++;
					}
				// next: seperate between 0-1 and 1-0 mistakes. 
					/*myfile << num_learned << "," << i << "," << (int)Reduce_Heuristic::LBD3_ClauseActivity << "," << h1 << endl;
					myfile << num_learned << "," << i << "," << (int)Reduce_Heuristic::LBD3_BCPActivity << "," << h2 << endl;
					myfile << num_learned << "," << i << "," << (int)Reduce_Heuristic::LBD3_BCPWActivity << "," << h3 << endl;
					*/
				}
			}
			// !!

			//!! if (ReduceHeuristic != Reduce_Heuristic::No_Reduce && num_learned % ReduceClauses == 0) reduce();
		}
		res = decide();
		if (res == SolverState::SAT || res == SolverState::UNSAT) {
			cout << "H1: " << accumulate(revived1.begin(), revived1.end(), 0)/revived1.size() << endl;
			cout << "H2: " << accumulate(revived2.begin(), revived2.end(), 0)/revived2.size() << endl;
			cout << "H3: " << accumulate(revived3.begin(), revived3.end(), 0)/revived3.size() << endl;
			return res;
		}
	}
}

#pragma endregion solving

void help() {
	stringstream st;
	st << "\nUsage: edusat <options> <file name>\n \n"
		"Options:\n";
	for (auto h : options) {
		void* varp;
		int lb, ub;
		string msg;
		tie(varp, lb, ub, msg) = options[h.first];
		st << left << setw(16) << "-" + h.first;
		st << msg << ". Default: " << *((int*)varp) << "." << endl;
	}
	Abort(st.str(), 3);
}

// The options are specified in edusat.h ('options'). 
void parse_options(int argc, char** argv) {
	if (argc < 2 || string(argv[1]).compare("-h") == 0)
		help();
	for (int i = 1; i < argc - 1; ++i) {
		Assert(argv[i][0] == '-');
		string st = argv[i] + 1;
		if (options.count(st) == 0) {
			cout << st << endl;
			Abort("Unknown flag ", 2);
		}
		if (i == argc - 2) Abort(string("missing value after ") + st, 2);
		i++;
		int val = stoi(argv[i]);
		
		void* varp;
		int lb, ub;
		string msg;
		tie(varp, lb, ub, msg) = options[st];
		if (val < lb || val > ub)
			Abort(st + string(" value should be in the range ") + to_string(lb) + "-" + to_string(ub), 2);
		*((int*)(varp)) = val; // This is where we assign the value. 
	}
	cout << argv[argc - 1] << endl;
}


#pragma region assumptions
void Solver::set_assumptions(vector<Lit> assump) {
	assumptions.clear();
	for (auto l : assump) {
		Assert(v2l(l) <= nvars);
		assumptions.push_back(v2l(l));
	}
	assumptions_dl = assumptions.size();
}

void Solver::read_assumptions(string filename) {
	assumptions.clear();
	std::ifstream infile(filename);
	int a, b;
	// Assumes the assumptions are like clauses with 1 variable, e.g. 
	// 100 0
	// means we assume the *variable* 100 to be true. 
	// This is why we push v2l(a).
	while (infile >> a >> b)
	{
		assumptions.push_back(v2l(a));	
	}
	infile.close();
	assumptions_dl = assumptions.size();
}

// Example: how to use assumptions
void SolveWithAssumptions(Solver& S) {
	vector<Lit> assumptions{1, 2, 3, 4};
	SolverState res;
	vector<Lit> ResponsibleAssump = assumptions;
	
	S.set_assumptions(assumptions);
	res = S.solve();
	if (res == SolverState::TIMEOUT) return;
	if (res == SolverState::UNSAT) {
		ResponsibleAssump = S.get_ResponsibleAssumptions();
	}
	if (ResponsibleAssump.size() > 0) {
		ResponsibleAssump.pop_back(); // remove one reason and try again: 
		S.set_assumptions(ResponsibleAssump);
		S.solve();
	}
}


void SolveWithAssumptionsFile(Solver& S, string filename) {	
	SolverState res;
	vector<Lit> ResponsibleAssump;
	cout << "Reading assumptions from " << filename << endl;
	S.read_assumptions(filename);
	res = S.solve();
	if (res == SolverState::TIMEOUT) return;
	// In case we want to do something with the assumptions responsible for the unsat: 
	/*if (res == SolverState::UNSAT) {
		ResponsibleAssump = S.get_ResponsibleAssumptions();
	}	*/
}

#pragma endregion assumptions
/******************  main ******************************/

int main(int argc, char** argv){
	begin_time = cpuTime();
	parse_options(argc, argv);
	
	ifstream in (argv[argc - 1]);
	if (!in.good()) Abort("cannot read input file", 1);	
	S.read_cnf(in);		
	in.close();
//	SolveWithAssumptions(S); 
//	SolveWithAssumptionsFile(S, "c:\\temp\\assumps.txt");
	S.solve();	

	return 0;
}
